Drupal.jsNodeload = {};
Drupal.jsNodeload.oldnids = new Array;

if (Drupal.jsEnabled) {
  $(document).ready(function() {
  Drupal.jsNodeload.autoAttach();
  });
}

Drupal.jsNodeload.autoAttach = function() {
  $("select.jsnodeload").each( function() {
      Drupal.jsNodeload.select($(this));
      });
  $("input.jsnodeload").each( function() {
      Drupal.jsNodeload.checkbox($(this));
      });
}

/**
 * Handler for the dropdown select
 */
Drupal.jsNodeload.select = function(selector) {
  var separator = selector.parents('fieldset.jsnodeload').find('input.separator');
  separator.data('oldnid', selector.val()); // preloading
  selector.click( function() {
    var target = $(this).parents('fieldset.jsnodeload').find("textarea");
    var nid = $(this).val();
    $.post(
      Drupal.settings.node_load.ajaxURL,
      {
        'separator': separator.val(),
        'nid': nid,
        'oldnid': separator.data('oldnid'),
        'body': target.val()
      },
      function (data) {
        target.val(data);
      });
      separator.data('oldnid', nid); // store the new node id
    });

}

/**
 * Handler for the checkbox list
 */
Drupal.jsNodeload.checkbox = function(selector) {
  selector.click( function() {
      // get the target textarea
      var separator = selector.parents('fieldset').find('input.separator');

      var target = $(this).parents('fieldset.jsnodeload').find("textarea");

      var action = this.checked ? 'load' : 'unload';
      $.post(Drupal.settings.node_load.ajaxURL,
        {'separator': separator.val(), 'nid': $(this).val(), 'action': action, 'body': target.val()},
        function (data) {
          target.val(data);
        });
      });
}
